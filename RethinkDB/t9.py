import sys
import time
from rethinkdb import RethinkDB

r = RethinkDB()

#Connect to subsequent commands
r.connect('localhost', 28015).repl()

#Def time
def get_time():
    return time.time()
    
#Init star-time
start = get_time()

with open ('input_polls.json') as f:
    file_data = json.load(f)

r.db("test_db").table("test").insert(file_data)

#Init end-time
end = get_time()-start

print("Sum:")
print(str(end))