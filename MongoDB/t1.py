#!/usr/bin/env python3

import pymongo
import time
import sys

#Connect to subsqeuent commands
client = pymongo.MongoClient(j=True)
db = client.test_db
coll = db.test_collection

#Def time
def get_time():
    return time.time()

#Init star-time
start = get_time()

#Drop Coll
coll.drop()

#Number of Documents
documentsNumber = int(sys.argv[1])

#Write
for i in range(0, documentsNumber):
        doc = {
        "autor_imie" : "Jan",
        "autor_nazwisko" : "Kowalski",
        "autor_wiek" : 15,
        "komentarz": "text"
        }
        coll.insert(doc)

#Init end-time
end = get_time()-start

print("Sum:")
print(str(end))