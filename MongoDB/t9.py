import pymongo
import time
import sys
import json

#Connect to subsqeuent commands
client = pymongo.MongoClient(j=True)
db = client.test_db
coll = db.test_collection

#Def time
def get_time():
    return time.time()

#Init star-time
start = get_time()

with open ('input_polls.json') as f:
    file_data = json.load(f)

coll.insert_many(file_data)

#Init end-time
end = get_time()-start

print("Sum:")
print(str(end))